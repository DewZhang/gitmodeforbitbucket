#ifndef _CIRCLE_H_
#define _CIRCLE_H_

#define PI 3.14

class Circle
{
private:
	float radius;
public:
	Circle();
	Circle(float r):radius(1){ radius = r;};
	float getRadius(void);
	void setRadius(float r);
	float getArea(void);
	float getPerimeter(void);
};



#endif


