#include "stdafx.h"
#include <iostream>
#include "circle.h"
using namespace std;

Circle::Circle()
{
	radius = 1;
}

float Circle::getRadius(void)
{
	return radius;
}

void Circle::setRadius(float r)
{
	radius = r;
}

float Circle::getArea(void)
{
	return (radius * radius * PI);
}

float Circle::getPerimeter(void)
{
	return (2 * PI * radius);
}
//123456


