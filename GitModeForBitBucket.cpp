// GitModeForBitBucket.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "circle.h"
using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	cout<<"Hello World"<<endl;
	Circle c(20);
	cout<<"================================"<<endl;
	cout<<"r = "<<c.getRadius()<<endl;
	cout<<"Perimeter = "<<c.getPerimeter()<<endl;
	cout<<"Area = "<<c.getArea()<<endl;

	cout<<"================================"<<endl;
	c.setRadius(10);
	cout<<"r = "<<c.getRadius()<<endl;
	cout<<"Perimeter = "<<c.getPerimeter()<<endl;
	cout<<"Area = "<<c.getArea()<<endl;

	cout<<"================================"<<endl;
	c.setRadius(20);
	cout<<"r = "<<c.getRadius()<<endl;
	cout<<"Perimeter = "<<c.getPerimeter()<<endl;
	cout<<"Area = "<<c.getArea()<<endl;

	cout<<"================================"<<endl;
	while(1);
	return 0;
}

